package ru.t1.semikolenov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.semikolenov.tm.command.AbstractCommand;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.dto.model.ProjectDTO;
import ru.t1.semikolenov.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return serviceLocator.getProjectEndpoint();
    }

    protected void showProject(@NotNull final ProjectDTO project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}